import {Token} from "./Token";
import {PermissionMetadata} from "./PermissionMetadata";

export interface PermissionToken extends Token {
    data: PermissionMetadata,
}

export const convertTokenToPermissionToken = (token: Token): PermissionToken => {
    return {
        ...token,
        data: {
            ...token.data,
            level: parseInt(token.data.data.data.level),
        }
    }
}