import {Metadata} from "./Metadata";

export interface RevokedPermissionMetadata extends Metadata {
    revokedTokenId: string;
}