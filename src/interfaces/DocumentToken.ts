import {DocumentMetadata} from "./DocumentMetadata";
import {Token} from "./Token";

export interface DocumentToken extends Token {
    data: DocumentMetadata,
}

export const convertTokenToDocumentToken = (token: Token): DocumentToken => {
    return {
        ...token,
        data: {
            ...token.data,
            URI: token.data.data.data.URI,
            description: token.data.data.data.description,
        }
    }
}