export interface Metadata {
    timestamp: number,
    creator: string,
    tokenType: TokenType,
    permissionTokenId: string,
    name: string,
    data: any,
}

export enum TokenType {
    Permission,
    PermissionEnd,
    Document
}