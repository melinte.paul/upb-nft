import {Nft} from "@alch/alchemy-web3/dist/esm/alchemy-apis/types";
import {Metadata} from "./Metadata";

export interface Token {
    tokenID: string,
    data: Metadata,
}

export const convertWeb3ResponseToToken = (Web3Token: Nft): Token | undefined => {
    try {
        const jsonData = JSON.parse(Web3Token.tokenUri?.raw || "");
        return {
            tokenID: Web3Token.id.tokenId,
            data: {
                timestamp: jsonData.timestamp,
                creator: jsonData.creator,
                tokenType: jsonData.data.tokenType,
                permissionTokenId: jsonData.data.permissionTokenId,
                name: jsonData.data.name,
                data: jsonData,
            },
        }
    } catch (err) {
        console.log(err);
    }
}