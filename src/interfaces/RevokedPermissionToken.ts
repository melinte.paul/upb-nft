import {Token} from "./Token";
import {RevokedPermissionMetadata} from "./RevokedPermissionMetadata";

export interface RevokedPermissionToken extends Token {
    data: RevokedPermissionMetadata,
}

export const convertTokenToRevokedPermissionToken = (token: Token): RevokedPermissionToken => {
    return {
        ...token,
        data: {
            ...token.data,
            revokedTokenId: token.data.data.data.revokedTokenId,
        }
    }
}