import {Metadata} from "./Metadata";

export interface DocumentMetadata extends Metadata {
    URI: string,
    description: string,
}