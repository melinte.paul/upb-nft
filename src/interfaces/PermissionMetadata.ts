import {Metadata} from "./Metadata";

export interface PermissionMetadata extends Metadata {
    level: PermissionLevel,
}

export enum PermissionLevel {
    Admin,
    University,
    Faculty,
    Secretary
}

export const CreatablePermissionsPerLevel: Record<String[PermissionLevel], PermissionLevel[]> = {
    [PermissionLevel.Admin]: [PermissionLevel.University, PermissionLevel.Faculty, PermissionLevel.Secretary],
    [PermissionLevel.University]: [PermissionLevel.Faculty, PermissionLevel.Secretary],
    [PermissionLevel.Faculty]: [PermissionLevel.Secretary],
}

export const CanCreatePermissions = (level: PermissionLevel) => level !== PermissionLevel.Secretary;