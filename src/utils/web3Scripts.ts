import {API_URL, CONTRACT_ADDRESS} from "../resources/consts";
import {Token} from "../interfaces/Token";

const {createAlchemyWeb3} = require("@alch/alchemy-web3");

const web3 = createAlchemyWeb3(API_URL);
const contractABI = require("../UPBNFT.json").abi;
const contract = new web3.eth.Contract(contractABI, CONTRACT_ADDRESS);

export const getTokens = async (addr: string) => {
    return web3.alchemy.getNfts({
        owner: addr,
        contractAddresses: [CONTRACT_ADDRESS],
        withMetadata: true,
    });
}

export const getTotalSupply = async (): Promise<number> => {
    let totalSupply = 0;

    await contract.methods.totalSupply().call((err: any, result: string) => {
        totalSupply = parseInt(result);
    });

    return totalSupply;
}

export const getAllTokens = async () => {
    const totalSupply = await getTotalSupply();
    const tokens: Token[] = [];

    for (let i = 0; i < totalSupply; i++) {
        await contract.methods.tokenURI(i).call((err: any, result: string) => {
            const jsonData = JSON.parse(result);

            return tokens.push({
                tokenID: i.toString(),
                data: {
                    timestamp: jsonData.timestamp,
                    creator: jsonData.creator,
                    tokenType: jsonData.data.tokenType,
                    name: jsonData.name,
                    permissionTokenId: jsonData.permissionTokenId,
                    data: jsonData,
                },
            });
        });
    }

    return tokens;
}

export const mintDocument = async (to: string, metadata: string) => {
    const tx = {
        'from': window.ethereum.selectedAddress,
        'to': CONTRACT_ADDRESS,
        'data': contract.methods.mintDocument(to, metadata).encodeABI()
    };

    try {
        const txHash = await window.ethereum
            .request({
                method: 'eth_sendTransaction',
                params: [tx],
            });
        return {
            success: true,
            status: "Check out your transaction on Etherscan: https://ropsten.etherscan.io/tx/" + txHash
        }
    } catch (error: any) {
        return {
            success: false,
            status: "Something went wrong: " + error.message
        }
    }
}

export const grantPermission = async (to: string, metadata: string) => {
    const tx = {
        'from': window.ethereum.selectedAddress,
        'to': CONTRACT_ADDRESS,
        'data': contract.methods.grantPermission(to, metadata).encodeABI()
    };

    try {
        const txHash = await window.ethereum
            .request({
                method: 'eth_sendTransaction',
                params: [tx],
            });
        return {
            success: true,
            status: "Check out your transaction on Etherscan: https://ropsten.etherscan.io/tx/" + txHash
        }
    } catch (error: any) {
        return {
            success: false,
            status: "Something went wrong: " + error.message
        }
    }
}

export const revokePermission = async (to: string, metadata: string) => {
    const tx = {
        'from': window.ethereum.selectedAddress,
        'to': CONTRACT_ADDRESS,
        'data': contract.methods.revokePermission(to, metadata).encodeABI()
    };

    try {
        const txHash = await window.ethereum
            .request({
                method: 'eth_sendTransaction',
                params: [tx],
            });
        return {
            success: true,
            status: "Check out your transaction on Etherscan: https://ropsten.etherscan.io/tx/" + txHash
        }
    } catch (error: any) {
        return {
            success: false,
            status: "Something went wrong: " + error.message
        }
    }
}