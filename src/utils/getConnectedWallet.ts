export const getConnectedWallet = async () => {
    if (window.ethereum) {
        try {
            const addressArray = await window.ethereum.request({
                method: "eth_accounts",
            });
            if (addressArray.length > 0) {
                return {
                    status: "Connected to address " + addressArray[0],
                    address: addressArray[0],
                };
            } else return {
                address: "",
                status: "No wallet connected",
            };
        } catch (err) {
            return {
                address: "",
                status: "Error: " + (err as any).message,
            };
        }
    } else {
        return {
            address: "",
            status: "Install metamask",
        };
    }
}