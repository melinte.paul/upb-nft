export const connectWallet = async () => {
    if (window.ethereum) {
        try {
            const addressArray: string[] = await window.ethereum.request({
                method: "eth_requestAccounts",
            });
            return {
                status: "Connected to address " + addressArray[0],
                address: addressArray[0],
            };
        } catch (err) {
            return {
                address: "",
                status: "Error: " + (err as any).message,
            };
        }
    } else {
        return {
            address: "",
            status: "Install metamask",
        };
    }
};
