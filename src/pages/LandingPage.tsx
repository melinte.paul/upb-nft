import {useContext} from "react";
import {Navigate} from "react-router";
import {LandingPageConnectButton} from "../components/LandingPageConnectButton";
import {AccountContext} from "../contexts/AccountContext";
import {SearchAddress} from "../components/SearchAddress";
import {Flex, Text} from "@chakra-ui/react";

export const LandingPage = () => {
    const walletAccount = useContext(AccountContext).walletAccount;

    return (
        <Flex flexDir="column" align="center">
            <Text fontSize="60px" my="32px">Welcome to UPB NFT</Text>
            <LandingPageConnectButton/>
            <SearchAddress my="32px"/>
            {walletAccount !== "" && <Navigate replace to="/account"/>}
        </Flex>
    );
};