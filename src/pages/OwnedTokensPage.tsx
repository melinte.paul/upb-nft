import {useContext} from "react";
import {TokensContext} from "../contexts/TokensContext";
import {TokensDisplay} from "../components/TokensDisplay";

export const OwnedTokensPage = () => {
    const walletDocuments = useContext(TokensContext).walletDocuments;
    const walletPermissions = useContext(TokensContext).walletPermissions;

    return (<TokensDisplay documentTokens={walletDocuments} permissionTokens={walletPermissions}/>);
};