import {Navigate, useParams} from "react-router";
import {useForm} from "react-hook-form";
import React, {useContext} from "react";
import {TokensContext} from "../contexts/TokensContext";
import {Box, Button, FormControl, FormErrorMessage, Input, Select} from "@chakra-ui/react";
import {CreatablePermissionsPerLevel, PermissionLevel} from "../interfaces/PermissionMetadata";
import {TokenType} from "../interfaces/Metadata";
import {grantPermission} from "../utils/web3Scripts";

export const CreatePermissionPage = () => {
    const {tokenID} = useParams();
    const permissionToken = useContext(TokensContext).walletPermissions.find(t => t.tokenID === tokenID);

    const {
        handleSubmit,
        register,
        formState: {errors, isSubmitting}
    } = useForm();

    const onSubmit = (values: any) => {
        grantPermission(values.address, JSON.stringify({
            tokenType: TokenType.Permission,
            permissionTokenId: tokenID,
            level: values.level,
            name: values.name,
        })).then(r => alert(r.status));
    }

    return (
        <Box width={"80%"}>
            {permissionToken &&
                <form onSubmit={handleSubmit(onSubmit)}>
                    <FormControl isInvalid={errors.name}>
                        <Box>
                            <Input mt={4} id="address" placeholder="Address" {...register("address")}/>
                            <Input mt={4} id="name" placeholder="Name" {...register("name")}/>
                            <Select mt={4} id="level" {...register("level")}>
                                {CreatablePermissionsPerLevel[permissionToken.data.level].map(p =>
                                    <option value={p} key={p}>
                                        {PermissionLevel[p]}
                                    </option>)}
                            </Select>
                            <FormErrorMessage>
                                {errors.name && errors.name.message}
                            </FormErrorMessage>
                        </Box>
                    </FormControl>
                    <Button mt={4} isLoading={isSubmitting} type="submit">Submit</Button>
                </form>}
            {!permissionToken && <Navigate replace to="/"/>}
        </Box>)
};