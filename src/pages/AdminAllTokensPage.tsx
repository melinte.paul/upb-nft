import {useEffect, useState} from "react";
import {getAllTokens} from "../utils/web3Scripts";
import {Token} from "../interfaces/Token";
import {Box, Text} from "@chakra-ui/react";

export const AdminAllTokensPage = () => {
    const [tokens, setTokens] = useState<Token[]>([]);

    useEffect(() => {
        getAllTokens().then(r => setTokens(r));
    }, [])

    return (
        <>
            {tokens.map(token => (
                <Box border="2px" mt={4} padding={4} key={token.tokenID} borderRadius="16px" width="80%">
                    <Text>ID: {token.tokenID}</Text>
                    <br/>
                    <Text>Metadata: {JSON.stringify(token.data.data)}</Text>
                </Box>
            ))}
        </>
    );
}