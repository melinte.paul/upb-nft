import {Navigate, useParams} from "react-router";
import React, {useContext, useRef, useState} from "react";
import {TokensContext} from "../contexts/TokensContext";
import {useForm} from "react-hook-form";
import {mintDocument} from "../utils/web3Scripts";
import {TokenType} from "../interfaces/Metadata";
import {Box, Button, FormControl, FormErrorMessage, Input, Textarea} from "@chakra-ui/react";
import {create} from "ipfs-http-client";
import {INFURA_PROJECT_ID, INFURA_PROJECT_SECRET} from "../resources/consts";

export const CreateDocumentPage = () => {
    const {tokenID} = useParams();
    const permissionToken = useContext(TokensContext).walletPermissions.find(t => t.tokenID === tokenID);
    const fileInput = useRef<any>();
    const [URI, setURI] = useState("");

    const ipfs = create({
        url: "https://ipfs.infura.io:5001/api/v0",
        headers: {
            authorization: "Basic " + btoa(INFURA_PROJECT_ID + ":" + INFURA_PROJECT_SECRET),
        },
    });

    const selectFile = () => {
        fileInput.current.click();
    }

    const handleSelectFile = async (event: any) => {
        const file: File = event.target.files[0];

        const result = await ipfs.add(file);

        setURI("https://upb-nft.infura-ipfs.io/ipfs/" + result.path);
    }

    const {
        handleSubmit,
        register,
        formState: {errors, isSubmitting}
    } = useForm();

    const onSubmit = (values: any) => {
        mintDocument(values.address, JSON.stringify({
            tokenType: TokenType.Document,
            permissionTokenId: tokenID,
            URI: URI,
            name: values.name,
            description: values.description,
        })).then(r => alert(r.status));
    }

    return (
        <Box width={"80%"}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <FormControl isInvalid={errors.name}>
                    <Box>
                        <Input mt={4} id="address" placeholder="Address" {...register("address")}/>
                        <Input mt={4} id="name" placeholder="Name" {...register("name")}/>
                        <input type="file" hidden multiple={false} ref={fileInput} onChange={handleSelectFile}/>
                        <Button mt={4} onClick={selectFile}>Upload file to IPFS</Button>
                        <Input mt={4} id="URI" placeholder="URI to document" value={URI}
                               onChange={(e) => setURI(e.target.value)}/>
                        <Textarea mt={4} id="description" placeholder="Description" {...register("description")}/>
                        <FormErrorMessage>
                            {errors.name && errors.name.message}
                        </FormErrorMessage>
                    </Box>
                </FormControl>
                <Button mt={4} isLoading={isSubmitting} type="submit">Submit</Button>
            </form>
            {!permissionToken && <Navigate replace to="/"/>}
        </Box>
    )
}