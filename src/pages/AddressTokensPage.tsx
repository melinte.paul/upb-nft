import {useParams} from "react-router";
import {useEffect, useMemo, useState} from "react";
import {getTokens} from "../utils/web3Scripts";
import {convertWeb3ResponseToToken, Token} from "../interfaces/Token";
import {convertTokenToPermissionToken, PermissionToken} from "../interfaces/PermissionToken";
import {TokenType} from "../interfaces/Metadata";
import {convertTokenToDocumentToken, DocumentToken} from "../interfaces/DocumentToken";
import {TokensDisplay} from "../components/TokensDisplay";
import {convertTokenToRevokedPermissionToken, RevokedPermissionToken} from "../interfaces/RevokedPermissionToken";

export const AddressTokensPage = () => {
    const {address} = useParams();
    const [tokens, setTokens] = useState<Token[]>([]);

    useEffect(() => {
        if (address) {
            getTokens(address).then(r => r.ownedNfts.map(convertWeb3ResponseToToken)).then(setTokens);
        } else {
            setTokens([]);
        }
    }, [address]);

    const revokedPermissions: RevokedPermissionToken[] = useMemo(
        () => (tokens.filter(t => t.data.tokenType === TokenType.PermissionEnd))
            .map(convertTokenToRevokedPermissionToken),
        [tokens]
    );

    const permissions: PermissionToken[] = useMemo(
        () => (tokens.filter(t => t.data.tokenType === TokenType.Permission)
            .map(convertTokenToPermissionToken)
            .filter(pt => revokedPermissions.filter(t => t.data.revokedTokenId === pt.tokenID).length === 0)),
        [tokens, revokedPermissions]);

    const documents: DocumentToken[] = useMemo(
        () => (tokens.filter(t => t.data.tokenType === TokenType.Document)
            .map(convertTokenToDocumentToken)),
        [tokens]);


    return (<TokensDisplay documentTokens={documents} permissionTokens={permissions}/>);
}