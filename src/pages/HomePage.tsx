import React from "react";
import {Box, Center, Text} from "@chakra-ui/react";

export const HomePage = () => {

    return (
        <Box>
            <Center>
                <Text fontSize="60px" my="32px">Welcome to UPB NFT</Text>
            </Center>
        </Box>
    );
};