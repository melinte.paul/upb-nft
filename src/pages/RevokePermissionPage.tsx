import {Navigate, useParams} from "react-router";
import React, {useContext, useState} from "react";
import {TokensContext} from "../contexts/TokensContext";
import {Box, Button, Flex, FormControl, FormErrorMessage, Input, Select} from "@chakra-ui/react";
import {convertTokenToPermissionToken, PermissionToken} from "../interfaces/PermissionToken";
import {getTokens, revokePermission} from "../utils/web3Scripts";
import {convertWeb3ResponseToToken, Token} from "../interfaces/Token";
import {TokenType} from "../interfaces/Metadata";
import {useForm} from "react-hook-form";

export const RevokePermissionPage = () => {
    const {tokenID} = useParams();
    const permissionToken = useContext(TokensContext).walletPermissions.find(t => t.tokenID === tokenID);
    const [address, setAddress] = useState("");
    const [permissionTokens, setPermissionTokens] = useState<PermissionToken[]>([]);

    const onSubmitSearch = () => {
        getTokens(address)
            .then(r => r.ownedNfts.map(convertWeb3ResponseToToken)
                .filter((t: Token) => t.data.tokenType === TokenType.Permission)
                .map(convertTokenToPermissionToken)
                .filter((pt: Token) => pt.data.permissionTokenId === tokenID))
            .then(r => setPermissionTokens(r));
    }

    const {
        handleSubmit,
        register,
        formState: {errors, isSubmitting}
    } = useForm();

    const onSubmit = (values: any) => {
        revokePermission(address, JSON.stringify({
            tokenType: TokenType.PermissionEnd,
            revokedTokenId: values.tokenID,
            permissionTokenId: tokenID,
        })).then(r => alert(r.status));
    }

    return (
        <Box width={"80%"}>
            {permissionToken && (<>
                <Box>
                    <Flex flexDir="row" align="center" mt={4}>
                        <Input id="address" placeholder="Address" mr={1}
                               onChange={(e) => setAddress(e.target.value)}/>
                        <Button onClick={onSubmitSearch}>Search</Button>
                    </Flex>
                    {permissionTokens && (
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <FormControl isInvalid={errors.name}>
                                <Select mt={4} id="tokenID" {...register("tokenID")}>
                                    {permissionTokens.map(pt =>
                                        <option value={pt.tokenID} key={pt.tokenID}>
                                            {"ID " + parseInt(pt.tokenID, 16) + ": " + pt.data.name}
                                        </option>)}
                                </Select>
                            </FormControl>
                            <FormErrorMessage>
                                {errors.name && errors.name.message}
                            </FormErrorMessage>
                            <Button mt={4} isLoading={isSubmitting} type="submit">Submit</Button>
                        </form>)}
                </Box>
            </>)}
            {!permissionToken && <Navigate replace to="/"/>}
        </Box>);
}