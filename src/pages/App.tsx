import React, {useEffect, useMemo, useState} from 'react';
import '../styles/App.css';
import {ChakraProvider} from "@chakra-ui/react";
import {Layout} from "../components/Layout";
import {Route, Routes} from "react-router";
import {LandingPage} from "./LandingPage";
import {AccountContext} from '../contexts/AccountContext';
import {getConnectedWallet} from "../utils/getConnectedWallet";
import {getTokens} from "../utils/web3Scripts";
import {TokensContext} from '../contexts/TokensContext';
import {convertWeb3ResponseToToken, Token} from "../interfaces/Token";
import {OwnedTokensPage} from "./OwnedTokensPage";
import {AdminAllTokensPage} from "./AdminAllTokensPage";
import {AdminCreateGenericToken} from "./AdminCreateGenericToken";
import {TokenType} from "../interfaces/Metadata";
import {convertTokenToDocumentToken, DocumentToken} from "../interfaces/DocumentToken";
import {convertTokenToPermissionToken, PermissionToken} from "../interfaces/PermissionToken";
import {CreateDocumentPage} from "./CreateDocumentPage";
import {CreatePermissionPage} from "./CreatePermissionPage";
import {AddressTokensPage} from "./AddressTokensPage";
import {RevokePermissionPage} from "./RevokePermissionPage";
import {convertTokenToRevokedPermissionToken} from "../interfaces/RevokedPermissionToken";
import {HomePage} from "./HomePage";

export const App = () => {
    const [walletAccount, setWalletAccount] = useState("");
    const [walletTokens, setWalletTokens] = useState<Token[]>([]);

    const addWalletListener = () => {
        if (window.ethereum) {
            window.ethereum.on("accountsChanged", (accounts: string[]) => {
                if (accounts.length > 0) {
                    setWalletAccount(accounts[0]);
                    console.log("Connected to address " + accounts[0]);
                } else {
                    setWalletAccount("");
                    console.log("Disconnected from all addresses");
                }
            })
        }
    }

    useEffect(() => {
        getConnectedWallet().then(res => {
            setWalletAccount(res.address);
            console.log(res.status);
        });

        addWalletListener();
    }, []);

    useEffect(() => {
        if (walletAccount !== "") {
            getTokens(walletAccount)
                .then(r => r.ownedNfts.map(convertWeb3ResponseToToken).filter((r: Token | undefined) => r !== undefined))
                .then(r => setWalletTokens(r));
        }
    }, [walletAccount]);

    const [walletPermissions, walletDocuments]: [PermissionToken[], DocumentToken[]] = useMemo(() => {
        const permissions = walletTokens.filter(t => t.data.tokenType === TokenType.Permission).map(convertTokenToPermissionToken);
        const documents = walletTokens.filter(t => t.data.tokenType === TokenType.Document).map(convertTokenToDocumentToken);
        const revokedPermissions = walletTokens.filter(t => t.data.tokenType === TokenType.PermissionEnd).map(convertTokenToRevokedPermissionToken);

        const availablePermissions = permissions
            .filter(pt => revokedPermissions
                .filter(t => t.data.revokedTokenId === pt.tokenID).length === 0);

        return [availablePermissions, documents];
    }, [walletTokens]);

    return (
        <ChakraProvider>
            <AccountContext.Provider value={{walletAccount, setWalletAccount}}>
                <TokensContext.Provider value={{walletPermissions, walletDocuments}}>
                    <Routes>
                        <Route path="/" element={<LandingPage/>}/>
                        <Route path="/account" element={<Layout/>}>
                            <Route index element={<HomePage/>}/>
                            <Route path="tokens" element={<OwnedTokensPage/>}/>
                            <Route path="admin">
                                <Route path="newToken" element={<AdminCreateGenericToken/>}/>
                                <Route path="tokens" element={<AdminAllTokensPage/>}/>
                            </Route>
                            <Route path="newDocument/:tokenID" element={<CreateDocumentPage/>}/>
                            <Route path="newPermission/:tokenID" element={<CreatePermissionPage/>}/>
                            <Route path="revokePermission/:tokenID" element={<RevokePermissionPage/>}/>
                            <Route path="searchAddress/:address" element={<AddressTokensPage/>}/>
                        </Route>
                        <Route path="/searchAddress/:address" element={<AddressTokensPage/>}/>
                    </Routes>
                </TokensContext.Provider>
            </AccountContext.Provider>
        </ChakraProvider>
    );
}

export default App;
