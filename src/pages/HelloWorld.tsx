import React from 'react';

interface Props {
    name: string;
}

const HelloWorld: React.FC<Props> = (props: Props) => {
    return (
        <h1>Hello world, {props.name}!</h1>
    )
}

export default HelloWorld;