import React from "react";
import {Box, Button, FormControl, FormErrorMessage, Input, Textarea} from "@chakra-ui/react";
import {useForm} from "react-hook-form";
import {mintDocument} from "../utils/web3Scripts";

export const AdminCreateGenericToken = () => {
    const {
        handleSubmit,
        register,
        formState: {errors, isSubmitting}
    } = useForm();

    const onSubmit = (values: any) => {
        mintDocument(values.address, values.metadata).then(r => alert(r.status));
    }

    return (
        <Box width={"80%"}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <FormControl isInvalid={errors.name}>
                    <Box>
                        <Input mt={4} id="address" placeholder="Address" {...register("address")}/>
                        <Textarea mt={4} id="metadata" placeholder="Token metadata" {...register("metadata")}/>
                        <FormErrorMessage>
                            {errors.name && errors.name.message}
                        </FormErrorMessage>
                    </Box>
                </FormControl>
                <Button mt={4} isLoading={isSubmitting} type="submit">Submit</Button>
            </form>
        </Box>

    )
}