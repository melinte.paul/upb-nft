import React from "react";
import {PermissionToken} from "../interfaces/PermissionToken";
import {DocumentToken} from "../interfaces/DocumentToken";

export interface TokensContextContent {
    walletPermissions: PermissionToken[],
    walletDocuments: DocumentToken[],
}

export const TokensContext = React.createContext<TokensContextContent>({walletPermissions: [], walletDocuments: []});