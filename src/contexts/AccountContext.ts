import React from "react";

export interface AccountContextContent {
    walletAccount: string,
    setWalletAccount?: (arg: string) => void,
}

export const AccountContext = React.createContext<AccountContextContent>({walletAccount: ""});