import React from "react";
import {DocumentToken} from "../interfaces/DocumentToken";
import {PermissionToken} from "../interfaces/PermissionToken";
import {DocumentDisplay} from "./DocumentDisplay";
import {PermissionDisplay} from "./PermissionDisplay";

interface Props {
    documentTokens: DocumentToken[],
    permissionTokens: PermissionToken[]
}

export const TokensDisplay: React.FC<Props> = (props: Props) => {
    return (
        <>
            {props.documentTokens.map(token => (
                <DocumentDisplay documentToken={token} key={token.tokenID}/>
            ))}
            {props.permissionTokens.map(token => (
                <PermissionDisplay permissionToken={token} key={token.tokenID}/>
            ))}
        </>
    );
}