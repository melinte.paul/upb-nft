import {Button} from "@chakra-ui/react";
import {connectWallet} from "../utils/walletConnect";
import {useContext} from "react";
import {AccountContext} from "../contexts/AccountContext";

export const LandingPageConnectButton = () => {
    const setWalletAccount = useContext(AccountContext).setWalletAccount;

    const connectWalletPressed = async () => {
        const walletResponse = await connectWallet();
        if (setWalletAccount) {
            setWalletAccount(walletResponse.address);
        }
        console.log(walletResponse.status);
    }

    return (
        <Button onClick={connectWalletPressed} width="200px">
            Connect
        </Button>
    )
}