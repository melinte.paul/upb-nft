import React, {useEffect, useState} from "react";
import {useForm} from "react-hook-form";
import {Button, Flex, Input, StyleProps} from "@chakra-ui/react";
import {Navigate} from "react-router";

interface Props {
    redirectPrefix?: string,
}

export const SearchAddress = ({redirectPrefix, ...props}: Props & StyleProps) => {
    const [address, setAddress] = useState("");

    const {
        handleSubmit,
        register,
    } = useForm();

    const onSubmit = (values: any) => {
        setAddress(values.address);
    }

    // eslint-disable-next-line
    useEffect(() => {
        setAddress("");
    });

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Flex {...props}>
                    <Input id="address" placeholder="Address" {...register("address")}/>
                    <Button type="submit">Search</Button>
                </Flex>
            </form>
            {address !== "" && <Navigate replace to={`${redirectPrefix || ""}/searchAddress/${address}`}/>}
        </>
    )

}