import {HStack, VStack} from "@chakra-ui/react";
import * as React from "react";
import {Outlet} from "react-router";

import {Sidebar} from "./Sidebar";

export const Layout = () => {

    return (
        <React.Fragment>
            <HStack height="100%">
                <Sidebar/>
                <VStack
                    height="100%"
                    width="80%"
                >
                    <Outlet/>
                </VStack>
            </HStack>
        </React.Fragment>
    );
};
