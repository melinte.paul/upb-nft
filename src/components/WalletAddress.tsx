import React, {useContext} from "react";
import {Text} from "@chakra-ui/react";
import {AccountContext} from "../contexts/AccountContext";
import {Navigate} from "react-router";

export const WalletAddress = () => {
    const walletAccount = useContext(AccountContext).walletAccount;

    return (
        <>
            <Text fontSize="xl">
                {"Connected: " + String(walletAccount).substring(0, 6) + "..." + String(walletAccount).substring(38)}
            </Text>
            {walletAccount === "" && <Navigate replace to="/"/>}
        </>
    )
}