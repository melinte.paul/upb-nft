import {PermissionToken} from "../interfaces/PermissionToken";
import {Box, Text} from "@chakra-ui/react";
import {PermissionLevel} from "../interfaces/PermissionMetadata";

interface Props {
    permissionToken: PermissionToken,
}

export const PermissionDisplay = (props: Props) => {
    return (
        <Box border="2px" mt={4} padding={4} borderRadius="16px" width="80%">
            <Text>ID: {parseInt(props.permissionToken.tokenID, 16)}</Text>
            <Text>Name: {props.permissionToken.data.name}</Text>
            <Text>Permission Level: {PermissionLevel[props.permissionToken.data.level]}</Text>
            <Text>Creator address: {props.permissionToken.data.creator}</Text>
        </Box>
    )
}