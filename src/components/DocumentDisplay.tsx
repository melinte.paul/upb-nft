import {DocumentToken} from "../interfaces/DocumentToken";
import {Box, Link, Text} from "@chakra-ui/react";

interface Props {
    documentToken: DocumentToken,
}

export const DocumentDisplay = (props: Props) => {
    return (
        <Box border="2px" mt={4} padding={4} borderRadius="16px" width="80%">
            <Text>ID: {parseInt(props.documentToken.tokenID, 16)}</Text>
            <Text>Name: {props.documentToken.data.name}</Text>
            <Text>Creator address: {props.documentToken.data.creator}</Text>
            <Text>Description: {props.documentToken.data.description}</Text>
            <Text>URI: {props.documentToken.data.URI}</Text>
            <Box border="1px" width="fit-content" padding={1} mt={2} borderRadius="8px">
                <Link href={props.documentToken.data.URI} isExternal>Go to URI</Link>
            </Box>
        </Box>
    );
}