import * as React from "react";
import {useContext, useMemo} from "react";
import {AccountContext} from "../contexts/AccountContext";
import {ADMIN_ADDRESS} from "../resources/consts";
import {CNavItem, CNavTitle, CSidebar, CSidebarHeader, CSidebarNav} from "@coreui/react";
import {Link} from "react-router-dom";
import {WalletAddress} from "./WalletAddress";
import {TokensContext} from "../contexts/TokensContext";
import {CanCreatePermissions} from "../interfaces/PermissionMetadata";
import {SearchAddress} from "./SearchAddress";

export const Sidebar = () => {
    const walletAccount = useContext(AccountContext).walletAccount;
    const walletPermissions = useContext(TokensContext).walletPermissions;

    const isAdmin = useMemo(() => (walletAccount === ADMIN_ADDRESS), [walletAccount]);

    return (
        <>
            <CSidebar style={{marginTop: 20, marginLeft: 30}}>
                <CSidebarHeader><WalletAddress/></CSidebarHeader>
                {isAdmin &&
                    <CSidebarNav>
                        <CNavTitle style={{listStyleType: "none"}}>Admin</CNavTitle>
                        <CNavItem><Link to="/account/admin/newToken">Create Token</Link></CNavItem>
                        <CNavItem><Link to="/account/admin/tokens">View all tokens</Link></CNavItem>
                    </CSidebarNav>
                }
                {walletPermissions.length > 0 &&
                    <CSidebarNav>
                        <CNavTitle style={{listStyleType: "none"}}>Create permissions</CNavTitle>
                        {walletPermissions.filter(token => CanCreatePermissions(token.data.level)).map(token => (
                            <CNavItem key={token.tokenID + "-grant-perm"}>
                                <Link to={`/account/newPermission/${token.tokenID}`}>
                                    As {token.data.name || token.tokenID}
                                </Link>
                            </CNavItem>))}
                    </CSidebarNav>
                }
                {walletPermissions.length > 0 &&
                    <CSidebarNav>
                        <CNavTitle style={{listStyleType: "none"}}>Revoke permissions</CNavTitle>
                        {walletPermissions.filter(token => CanCreatePermissions(token.data.level)).map(token => (
                            <CNavItem key={token.tokenID + "-revoke-perm"}>
                                <Link to={`/account/revokePermission/${token.tokenID}`}>
                                    As {token.data.name || token.tokenID}
                                </Link>
                            </CNavItem>))}
                    </CSidebarNav>
                }
                {walletPermissions.length > 0 &&
                    <CSidebarNav>
                        <CNavTitle style={{listStyleType: "none"}}>Create documents</CNavTitle>
                        {walletPermissions.map(token => (
                            <CNavItem key={token.tokenID + "-mint-document"}>
                                <Link to={`/account/newDocument/${token.tokenID}`}>
                                    As {token.data.name || token.tokenID}
                                </Link>
                            </CNavItem>))}
                    </CSidebarNav>
                }
                <CSidebarNav>
                    <CNavTitle style={{listStyleType: "none"}}>Wallet data</CNavTitle>
                    <CNavItem><Link to="/account/tokens">View owned Tokens</Link></CNavItem>
                </CSidebarNav>
                <CSidebarNav>
                    <SearchAddress redirectPrefix="/account" mt="32px"/>
                </CSidebarNav>
            </CSidebar>
        </>
    );
};
