// alchemy-nft-api/alchemy-web3-script.js
require('dotenv').config();
const {createAlchemyWeb3} = require("@alch/alchemy-web3");

async function get_test() {
    const API_URL = process.env.API_URL;
    const PUBLIC_KEY = process.env.PUBLIC_KEY;

// Initialize an alchemy-web3 instance:
    const web3 = createAlchemyWeb3(API_URL);

// The wallet address we want to query for NFTs:
    const ownerAddr = PUBLIC_KEY;
    const nfts = await web3.alchemy.getNfts({
        owner: ownerAddr
    })

// Print owner's wallet address:
    console.log("fetching NFTs for address:", ownerAddr);
    console.log("...");

// Print total NFT count returned in the response:
    console.log("number of NFTs found:", nfts.totalCount);
    console.log("...");

// Print contract address and tokenId for each NFT:
    for (const nft of nfts.ownedNfts) {
        console.log("===");
        console.log("contract address:", nft.contract.address);
        console.log("token ID:", nft.id.tokenId);
    }
    console.log("===");

// Fetch metadata for a particular NFT:
    console.log("fetching metadata for a crypto coven NFT...");
    const response = await web3.alchemy.getNftMetadata({
        contractAddress: "0x914caf3252c8fd0ae16fb2474d34c275f863048c",
        tokenId: "5"
    })

// Uncomment this line to see the full api response:
    console.log(response);

// Print some commonly used fields:
//console.log("NFT name: ", response.title);
//console.log("token type: ", response.id.tokenMetadata.tokenType);
//console.log("tokenUri: ", response.tokenUri.gateway);
//console.log("image url: ", response.metadata.image);
//console.log("time last updated: ", response.timeLastUpdated);
//console.log("===");
}

get_test();