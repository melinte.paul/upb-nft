const {ethers} = require("hardhat");

async function main() {
    // Grab the contract factory
    const contract = await ethers.getContractFactory("UPBNFT");

    // Start deployment, returning a promise that resolves to a contract object
    const deployed = await contract.deploy(); // Instance of the contract

    await deployed.deployed();

    console.log("Contract deployed to address:", deployed.address);
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
    });