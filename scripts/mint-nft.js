require('dotenv').config();
const {createAlchemyWeb3} = require("@alch/alchemy-web3");

const API_URL = process.env.API_URL;
const PUBLIC_KEY = process.env.PUBLIC_KEY;
const PRIVATE_KEY = process.env.PRIVATE_KEY;

const web3 = createAlchemyWeb3(API_URL);

const contract = require("../artifacts/contracts/UPB_NFT.sol/UPBNFT.json");
const contractAddress = "0xF8aA0b23b0781313098a155Af3C93943f5E9Bedb";
const nftContract = new web3.eth.Contract(contract.abi, contractAddress);

const metadataJSON = "{\"tokenType\":2,\"permissionTokenId\":\"0x0000000000000000000000000000000000000000000000000000000000000000\",\"URI\":\"test\",\"name\":\"Test\",\"description\":\"test\"}"


async function mintNFT(metadata) {
    const nonce = await web3.eth.getTransactionCount(PUBLIC_KEY, 'latest'); //get latest nonce

    //the transaction
    const tx = {
        'from': PUBLIC_KEY,
        'to': contractAddress,
        'nonce': nonce,
        'gas': 500000,
        'maxPriorityFeePerGas': 2999999987,
        'data': nftContract.methods.mintDocument(PUBLIC_KEY, metadata).encodeABI()
    };

    const signedTx = await web3.eth.accounts.signTransaction(tx, PRIVATE_KEY);
    const transactionReceipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);

    console.log(`Transaction receipt: ${JSON.stringify(transactionReceipt)}`);
}

async function mintNFTs(metadata, N) {
    for (let i = 0; i < N; i++) {
        await mintNFT(metadataJSON);
    }
}

mintNFTs(metadataJSON, 100);

