// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract UPBNFT is ERC721, ERC721Enumerable, ERC721URIStorage {
    using Counters for Counters.Counter;

    Counters.Counter private _tokenIdCounter;

    mapping(address => Counters.Counter) private _permCount;

    constructor() ERC721("UPB NFT", "UPB") {
        safeMint(msg.sender, '{"level": "0","tokenType": 0,"permissionTokenId": "0","name":"Admin"}');
        _permCount[msg.sender].increment();
    }

    function safeMint(address to, string memory uri) internal {
        uint256 tokenId = _tokenIdCounter.current();

        string memory timestamp = Strings.toString(block.timestamp);
        string memory from = Strings.toHexString(uint256(uint160(msg.sender)));

        string memory finalUri = string(abi.encodePacked('{"timestamp":"', timestamp, '","creator":"', from, '","data":', uri, '}'));

        _tokenIdCounter.increment();
        _safeMint(to, tokenId);
        _setTokenURI(tokenId, finalUri);
    }

    function grantPermission(address to, string memory uri) public {
        require(_permCount[msg.sender].current() > 0, "Not permitted");
        safeMint(to, uri);
        _permCount[to].increment();
    }

    function revokePermission(address to, string memory uri) public {
        require(_permCount[msg.sender].current() > 0, "Not permitted");
        safeMint(to, uri);
        _permCount[to].decrement();
    }

    function mintDocument(address to, string memory uri) public {
        require(_permCount[msg.sender].current() > 0, "Not permitted");
        safeMint(to, uri);
    }


    // The following functions are overrides required by Solidity.

    function _beforeTokenTransfer(address from, address to, uint256 tokenId) internal override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage) {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId) public view override(ERC721, ERC721URIStorage)
    returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function supportsInterface(bytes4 interfaceId) public view override(ERC721, ERC721Enumerable)
    returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}
